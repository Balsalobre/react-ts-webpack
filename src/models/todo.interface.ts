export default interface Todo {
    text: string;
    complete: boolean;
    date: Date;
}