import React, { useContext } from "react";
import { StrictMode } from "react";
import ReactDOM from "react-dom";

const Store = React.createContext({} as any);

export function Parent(props) {
  const obj = { text: 'Random text' };
  return <Store.Provider value={obj}>{props.children}</Store.Provider>;
}

export function Child() {
  const hook = useContext(Store);
return <div>{ hook.text }</div>;
}

export default function App() {
  return (
    <div className="App">
      <Parent>
        <Child></Child>
      </Parent>
    </div>
  );
}

const rootElement = document.getElementById("app-root");
ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  rootElement
);